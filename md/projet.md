# Mode Projet #
__Projet__
[//]: # (First page)

powered by :
[reveal JS](https://github.com/hakimel/reveal.js)
[TOC Progress](http://e-gor.github.io/Reveal.js-TOC-Progress/)

[cours pr�c�dent](lib/pdf/SI7_Projet.pdf)


# Plan

- Fondamentaux des projets
- L'essentiel pour d�marrer
- outils avanc�s pour les projet
- Outils Informatiques
- Analyse Fonctionnelle
- Planification Avanc�e
- Gestion de projet Agile avec Scrum



# Fondamentaux des projets

- D�finition
- Pourquoi un projet ?
- Paradoxe 
- Projet vs op�ration
- Organisation
- Co�t 
- Acteurs


## D�finition ##

- Temporaire: un d�but une fin

- Par �tapes, qui se pr�pare

- R�sultat =  Le livrable


## Pourquoi un projet ? ##

- Productivit� 

cr�ation de valeur / co�t

- Cr�ation de valeur 

Quantit� de valeur que l'on arrive � cr�er 

- Co�t 

Ressources consomm�es pour cr�er la valeur.


### Augmentation de la productivit� ### 

-   Baisse des co�ts :

--  mondialisation !

-  Augmentation de la cr�ation de valeur 

++ innovation et de l'am�lioration continue.

note: Plusieurs solutions existent afin d?augmenter la productivit�.



## Paradoxe ##

![paradoxe](img/projet/paradoxe.png)

Le lien entre la capacit� d'action et la connaissance d'un projet dans le temps. 

Note: Un bon moyen pour se souvenir de ce paradoxe : penser � l?adage populaire


## Projet vs Op�rations ##

Op�rations -> Activit�s r�p�titives, des ?processus?. � l?oppos�, des projets (innovants, uniques)


## Organisation ##
![orga](img/projet/orga_projet.png)


## Co�ts ##
![cout](img/projet/cout_projet.png)


## Acteurs ##

- Ma�trise d'ouvrage MOA

Client
- Ma�trise d'�uvre MOE

Conducteur de travaux

Note: 
Ma�trise d?ouvrage MOA = le client du projet, celui qui d�finit les objectifs et le budget et repr�sente les utilisateurs finaux. Exemple : lancer un appel d?offre.

Ma�trise d?�uvre MOE traduit en termes techniques le besoin exprim� par le MOA. Il conduit les travaux op�rationnels. Ce qui implique une coordination des acteurs, notamment en cas de sous-traitance (d�l�gation d?une partie du travail � un acteur diff�rent).


## Conclusion ##
- Anticiper , �tre proactif
- �tre flexible
- profil g�n�raliste ( technique, juridique, rh )


## Pour aller  plus loin ## 

- L'essentiel pour d�marrer
- Outils avanc�s pour les projets
- Outils Informatiques
- Analyse Fonctionnelle
- Planification Avanc�e
- Gestion de projet Agile avec Scrum


![cp](img/projet/justifier.png)


# sources 

[Mooc Gestion de projet](https://gestiondeprojet.pm/organisation-des-projets/)