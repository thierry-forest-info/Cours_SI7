# PCA #
__Strat�gies et techniques associ�es � la continuit� de service__
[//]: # (First page)

powered by :
[reveal JS](https://github.com/hakimel/reveal.js)
[TOC Progress](http://e-gor.github.io/Reveal.js-TOC-Progress/)


# Plan #

- Synth�se 
- Lexique
- D�marche Global
- Cas Sp�cifique des Syst�mes d'information
- Avantages d'un PCA 
- Points d'attention
- Validation du PCA 
- Aspect Budg�taire
- R�le des Assureurs



## Synth�se ##

- Importance d'un PCA    
- Co�t/avantage d'un PCA
- Contenu d'un PCA
- Conseil de r�alisation d'un PCA


### Importance d'un PCA ### 

- Vuln�rabilit�s + Menaces --> Sinistre

- Sinistre --> d�sastre Economique & perte d'image

Note: Perte de confiance des clients, profit de la concurence 


### Co�t & avantage d'un PCA ###

- Mise en oeuvre == co�ts directs --> r�sultat de l'entreprise
        
- N�gociation avec la soci�t� d'assurance 

- Optimisation de fonctionnement --> Efficacit�

Note: Meilleure connaissance de l'entreprise
de r�activit� 


### Contenu d'un PCA ###

- PCA = PCS + PCM + PRA 

- Disponibilit� des ressources mat�riels + ressources humaine ( personnes clefs)

- Actions � suivre par les m�tiers pour la poursuite d'activit�

- Proc�dures de redemarrage en cas de sinistre


### Cat�gories de risques ###

- Risques physiques classiques ( incendie, malveillance, etc..)

- Risques �mergents ( gouvernance / r�glementation / obsolescence technique)

Note:   Plan de Continuit� de service Plan de continuit� M�tier, Plan de reprise d'activit� 


### Conseil de r�alisation d'un PCA ###

- r�flexion strat�gique de l'entreprise (SWOT)

Note: M�thode SWOT


#### M�thode SWOT ####

![swot](img/SWOT_grapheFL.jpg)


#### Cartographie des processus #### 

- Amont ( fournisseurs )
- Sous traitants
- Aval ( Client )


#### R�alisation de Sc�narii des risques####

- majeurs/ essentiel 
- Internes / Externes)
- ciblage des �l�ments � prendre en compte


#### D�finition des objectifs ####
- Services � maintenir 
- Date limite de reprise d'activit�
- contraintes r�gelementaires � respecter

PCA non test� == inexistant


#### R�gles � suivre ####

- impliquer la Direction G�n�rale
- ne pas mettre en danger l'entreprise
- validations progressives
- suivi des am�liorations n�cessaires, souhaitables et possibles



## Pr�ambule ##

*" sur 4 entreprises qui subissent un incendie, pr�s de 3 ne reprennent pas leur activit� "* 

**source : dossier pour le salon Pr�ventica ( Lille du 7 au 9 juin 2016 )**

*"�tre conscient de la difficult� permet de l'�viter"*

**Lao Tseu**



## Lexique ##
AFNOR
ISO 22301
ISO 31000

Notes :Terme � pr�senter � la classe ( etudiant)



### D�marche d' �laboration d'un plan de continuit� ### 

![pca](img/DPCA.png)


### D�marche Globale  ###

- Cartographie des proc�d�s:
     Amont - Interm�diaires - Aval
- Hi�rarchisation des risques:
    Niveau d'impact
    + Mesure de r�duction de vuln�rabilit� 
        + Risque inaceptable
- D�finition de priorit�s 

" Le risque z�ro n'existe pas ! "


### Illustration ###

![Pas d'image](img/DGPCA.png)


### Principe d'am�lioration continue ###

![Pas d'image](img/Deming.png)



## Cas sp�cifique des syst�mes d'information ##

![Pas d'image](img/SIPCA.png)

Note: Organisation support� par des dispositifs d'exception


## Dispositifs d'exception ##

- une organisation de crise g�n�rale de l'entreprise, int�grant des sous-volets de gestion de crise par sous-syst�mes, dont le Syst�me d'Information (SI) ;
- un PCA M�tiers (PCM) int�grant des palliatifs m�tiers pour maintenir l'activit� pendant la remise en �tat ;
- un PRA informatique int�grant des solutions mat�rielles et organisationnelles pour reconstruire rapidement la partie du syst�me d'information vitale pour l'entreprise.


## R�le du PRA ##

Mettre � disposition dans des d�lais acceptables et convenu des moyens ad�quats hors zone sinistr�e
Les redondances de moyens doivent s'adapte r� la fr�quence et � la gravit� des risques.


## Sc�narios de risques envisag� dans un PRA ?## 

![Pas d'image](img/catmenaces.png)



### Avantages d'un PCA ###

- Limiter les cons�quences d'un sinistre
- Mieux conna�tre l'entreprise
- Optimiser / simplifier les processus de l'entreprise
- Atout commercial



### Points d'attention ###

- D�mythifier la d�marche d'un PCA
- Le PCA peut �tre limit� dans son ampleur
- Ressources humaines
    + Ressources humaines dont la d�faillance justifie un PCA
    + Ressources humaines n�cessaires � l'ex�cution du PCA
- Interconnexion de plusieurs PCA
- Cyber attaque
- Confidentialit�
- Ressources uniques et non rempla�ables



### Validation du PCA ###

- Le PCA fonctionnera-t-il en cas de besoin ?
- Les questions � se poser
- Quatre re?gles � suivre


## Le PCA fonctionnera-t-il en cas de besoin ? ##
    
- Production de preuves d'efficacit� issues de tests p�riodiques


## Les questions a? se poser ##

- Condition de test r�aliste
- � qui est destin�e la preuve ?
- peut-on �tre convainquant � 100%
- Pertinence  de test.
- Que veut-on couvrir ? Quels Domaines ?


## Quatre r�gles � suivre ##

- .
- .
- . 
- .

Note: Impliquer la direction G�n�rale
ne pas mettre en danger l'entreprise
opter pour des validations progressives
noter r�guli�rement les am�liorations n�cessaires, souhaitables et possible 



## Aspect Budgetaire ##

- Co�t d'un PCA 
- Analyse co�t/b�n�fice d'un PCA, ROI


## Co�t d'un PCA ##

+ co�t d'�tude
+ co�t de fonctionnement
+ co�t de mise en oeuvre 


## Analyse co�t/b�n�fice ## 

![Pas d'image](img/FinalPCA.png)



## Role des assureurs ## 

- Conseil en amont
- Assistance et intervention en Aval 



# En pratique # 

- Plan de continuit� de service
- Strat�gie de secours
- Plan de Secours Informatique
- Solution de secours


## Plan de continuit� de service ## 

Le PCS contient � la fois un PSI et un PRA. 

PCS : Plan de continuit� de service
PSI : Plan de secours informatique
PRA : Plan de reprise d'activit� 


### Objet ###
Clarification des risques d'indisponibilit� totale ou partielle du syst�me d'information
    
![Pas d'image](img/PCS.png)

### Action � mettre en oeuvre ###

- Inventaire des �l�ments du syst�me d'information indispensables � la poursuite de l'activit� de l'entreprise
- D�finition des exigences de continuit� de chacunes des activit�s 
- Identification des activit�s essentielles et �valuation des cons�quences d'int�ruption ou de d�gradation de service 
 

## Bilan des cons�quences directes ## 

- dur�e d'indisponibilit� des moyens
- de perte d'information
- potentialit� de risque 

***4 � 5 crit�res de gravit�s***
***D�sastre ---> simple arr�t***

## Conclusion ##

- Une fois de bilan r�alis�, il est possible d'�valuer les moyens de secours adapt�s et des sc�narios de reprise  pour ramener l'impact estim� � un niveau acceptable.

## Strat�gie de secours ## 

- D�finitions
- D�marches
- Phase de lancement 
- Phase d'�tude fonctionnelle
- Phase d'�tude de vuln�rabilit� 
- Phase d'analyse des risques
- Phase d'orientation


## D�finitions ## 

R�flexion issues des m�thodes MARION ou MEHARI
- Impact ( niveaux d�finis par l'entreprise ) 
- Potentialit� ( 4 � 5 niveaux )
- Gravit� ( Impact x Portentialit�) 

Note: degr� d'acceptation


## D�marche ##

![Pas d'image](img/FinalPCA.png)


## Phase de lancement ##

Mise en oeuvre d'un PSI
n�gociation avec la direction des risques qu'elle souhaite couvrir.
cf: guide d'analyse des risques

## Phase d'�tude fonctionnelle ## 

- arr�t temporaire ou d�finitif
- perte de donn�es
- d�gradation de services


### D�pendances d'une activit� ### 

![Pas d'image](img/dep_activity.png)



## Phase d'�tude de vuln�rabilit� ##

![Pas d'image](img/vul.png)


## Phase d'analyse des risques ##

- Etape technique d'�tude des sc�narios de sinistres
- Etapes fonctionnelle d'�tude d'impact


### Objectifs ###

Expression des cons�quences de :
- Dur�e d'indisponibilit� des moyens
- perte d'information & potentialit� du risque


## Phase d'orientation ##

Analyse de l'organigramme fonctionnel du syst�me d'information
Estimation de la dur�e d'int�rruption de service a chaque fonction vitale
Identification et regroupement n�cessaires au fonctionnement des fonctions vitales




## Plan de Secours Informatique ## 

- Introduction
- Organisation
- D�clenchement


## Introduction ##

Ce plan de secours permettra l �laboration d'un cahier des charges pour l'�tude de solutions. 
- Plan de pr�paration = mise en place des solutions retenue
- phase d'etude d�taill�s:
+ mise en place 
+ test 
+ formalisation des proc�dures op�rationnelles


## Organisation ##

- comit� de crise
- cellule de coordination
- �quipes d'intervention
- services utilisateurs


## D�clenchement ##

![start](img/startup.png)


## Gain du PCA ##

![Pas d'image](img/mise_en_oeuvre.png)


## Dispositif de secours ##

![Pas d'image](img/dispositif.png)


## Documentation ##

Volumineuse & confidentielle

Structur�e en 4 niveaux:
- Communiquer
- Mettre en oeuvre
- G�rer
- Contr�ler


## Maintenance du plan ##


![Pas d'image](img/rpsi.png)

![Pas d'image](img/outil_de_gestion.png)


## Plan de test ##

Trois cat�gories de test:
- Test technique unitaire
- Test d'int�gration
- Test en vraie grandeur



# Sources #

[imdr.fr](www.imdr.fr)

[iesf - Cahier 24](http://home.iesf.fr/offres/doc_inline_src/752/Cahier_24_PCA.pdf)

[Iso 31000](https://www.iso.org/fr/iso-31000-risk-management.html)

[clusif](https://clusif.fr/publications/plan-de-continuite-dactivite-strategie-et-solutions-de-secours-du-s-i/)